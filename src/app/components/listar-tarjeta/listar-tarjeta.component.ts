import { Component, OnInit } from '@angular/core';
import { TarjetaService } from '../../services/tarjeta.service';
import { TarjetaCredito } from '../../models/ModeloCredito';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-listar-tarjeta',
  templateUrl: './listar-tarjeta.component.html',
  styleUrls: ['./listar-tarjeta.component.css']
})
export class ListarTarjetaComponent implements OnInit {
  listTarjetas : TarjetaCredito[] = [];

  constructor(private _tarjetaService: TarjetaService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.obtenerTarjetas();
  }

  obtenerTarjetas(){
    this._tarjetaService.obtenerTarjetas().subscribe(doc => {
      console.log(doc);
      this.listTarjetas = [];
      doc.forEach((element : any) => {
        this.listTarjetas.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data()
        })
      });
      console.log(this.listTarjetas);
    })
  }

  eliminarTarjeta(id : any) {
    this._tarjetaService.eliminarTarjeta(id).then(()=> {
      this.toastr.error('La tarjeta fue eliminada con éxito','Registro Eliminado');
    }, error => {
        this.toastr.error('No se pudo borrar la tarjeta!','Error');
        console.log(error);
    })
  }

  editarTarjeta(tarjeta : TarjetaCredito) {
    this._tarjetaService.addTarjetaEdit(tarjeta);
  }
}
