// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAJtGjv0O2cI7HUzjG7DC9W984vUJFu7ks",
    authDomain: "tarjetacredito-c488a.firebaseapp.com",
    projectId: "tarjetacredito-c488a",
    storageBucket: "tarjetacredito-c488a.appspot.com",
    messagingSenderId: "789197708701",
    appId: "1:789197708701:web:0af131e1207abb648da374"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
